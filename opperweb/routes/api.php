<?php

use App\Http\Controllers\PostController;
use App\Http\Controllers\CategoriaController;
use App\Http\Controllers\ComentarioController;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

///////////////////////// Rutas para el controlador PostController  /////////////////////

Route::post('/post', [PostController::class, 'create']);
Route::put('/post/{id}', [PostController::class, 'update']);
Route::delete('/post/{id}', [PostController::class, 'delete']);
Route::get('/post/{id}/edit', [PostController::class, 'edit']);
Route::get('/post/{id}', [PostController::class, 'show']);
Route::get('/post', [PostController::class, 'showAll']);
Route::get('post/comentarios/{id}', [PostController::class, 'getCommentsByPost']);

//////////////////////   Rutas para el controlador CategoriaController /////////////////////

Route::post('/categoria', [CategoriaController::class, 'create']);
Route::put('/categoria/{id}', [CategoriaController::class, 'update']);
Route::delete('/categoria/{id}', [CategoriaController::class, 'delete']);
Route::get('/categoria/{id}/edit', [CategoriaController::class, 'edit']);
Route::get('/categoria/{id}', [CategoriaController::class, 'show']);
Route::get('/categoria', [CategoriaController::class, 'showAll']);

///////////////////////// Rutas para el controlador ComentarioController //////////////////

Route::post('/comentario', [ComentarioController::class, 'create']);
Route::put('/comentario/{id}', [ComentarioController::class, 'update']);
Route::delete('/comentario/{id}', [ComentarioController::class, 'delete']);
Route::get('/comentario/{id}/edit', [ComentarioController::class, 'edit']);
Route::get('/comentario/{id}', [ComentarioController::class, 'show']);
Route::get('/comentario', [ComentarioController::class, 'showAll']);


