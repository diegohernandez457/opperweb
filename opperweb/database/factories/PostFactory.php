<?php

namespace Database\Factories;

use App\Models\Categoria;
use App\Models\Post;
use Illuminate\Database\Eloquent\Factories\Factory;

class PostFactory extends Factory
{
    
    protected $model = Post::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'categoria_id' => Categoria::factory(),
            'titulo' => $this->faker->word(),
            'contenido' => $this->faker->sentence(10),

        ];
    }
}
