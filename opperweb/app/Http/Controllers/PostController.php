<?php

namespace App\Http\Controllers;

use App\Models\Comentario;
use App\Models\Post;
use Illuminate\Http\Request;


class PostController extends Controller
{

    public function create(Request $request)
    {
        $post = new Post();

        $post->categoria_id = $request->categoria;
        $post->titulo = $request->titulo;
        $post->contenido = $request->contenido;

        $post->save();

        return response()->json([
            '$post' => $post,
        ]);
    }

    public function update(Request $request)
    {
        $post = Post::findOrFail($request->id);

        $post->categoria_id = $request->categoria;
        $post->titulo = $request->titulo;
        $post->contenido = $request->contenido;

        $post->save();

        return response()->json([
            'post' => $post,
        ]);

    }
    public function delete(Request $request)
    {
        $post = Post::destroy($request->id);

        return response()->json([
            'response' => "Post Eliminado",
        ]);
    }

    public function edit(Request $request)
    {
        $post = Post::findOrFail($request->id);
        
        return response()->json([
            'post' => $post,
        ]);
    }

    public function show(Request $request)
    {
        $post = Post::findOrFail($request->id);

        return response()->json([
            'post' => $post,
        ]);
    }
    public function showAll()
    {
        $posts = Post::all();

        return response()->json([
            'posts' => $posts,
        ]);
    }

    public function getCommentsByPost(Request $request)
    {
        $comentarios = Comentario::where('post_id', $request->id)->get();
        return response()->json([
            'comentarios' => $comentarios,
        ]);
    }

}
