<?php

namespace App\Http\Controllers;

use App\Models\Categoria;
use Illuminate\Http\Request;

class CategoriaController extends Controller
{

    public function create(Request $request)
    {
        $categoria = new Categoria();

        $categoria->nombre = $request->nombre;

        $categoria->save();

        return response()->json([
            'categoria' => $categoria,
        ]);
    }

    public function update(Request $request)
    {
        $categoria = Categoria::findOrFail($request->id);

        $categoria->nombre = $request->nombre;

        $categoria->save();

        return response()->json([
            "categoria" => $categoria,
        ]);

    }
    public function delete(Request $request)
    {
        $categoria = Categoria::destroy($request->id);

        return response()->json([
            'respuesta' => "Categoria Eliminada",
        ]);
    }

    public function edit(Request $request)
    {
        $categoria = Categoria::findOrFail($request->id);
        
        return response()->json([
            'categoria' => $categoria,
        ]);
    }

    public function show(Request $request)
    {
        $categoria = Categoria::findOrFail($request->id);
        
        return response()->json([
            'categoria' => $categoria,
        ]);
    }
    public function showAll()
    {
        $categorias = Categoria::all();

        return response()->json([
            'categorias' => $categorias,
        ]);
    }
}
