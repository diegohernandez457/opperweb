<?php

namespace App\Http\Controllers;

use App\Models\Comentario;
use Illuminate\Http\Request;

class ComentarioController extends Controller
{
    public function create(Request $request)
    {
        $comentario = new Comentario();

        $comentario->post_id = $request->post;
        $comentario->contenido = $request->contenido;

        $comentario->save();

        return response()->json([
            'comentario' => $comentario,
        ]);
    }

    public function update(Request $request)
    {
        $comentario = Comentario::findOrFail($request->id);

        $comentario->post_id = $request->post;
        $comentario->contenido = $request->contenido;

        $comentario->save();

        return response()->json([
            'comentario' => $comentario,
        ]);
    }
    public function delete(Request $request)
    {
        $comentario = Comentario::destroy($request->id);

        return response()->json([
            'respuesta' => "Comentario Eliminado",
        ]);
    }

    public function edit(Request $request)
    {
        $comentario = Comentario::findOrFail($request->id);
        
        return response()->json([
            'comentario' => $comentario,
        ]);
    }

    public function show(Request $request)
    {
        $comentario = Comentario::findOrFail($request->id);
        
        return response()->json([
            'comentario' => $comentario,
        ]);
    }
    public function showAll()
    {
        $comentarios = Comentario::all();

        return response()->json([
            'comentarios' => $comentarios,
        ]);
    }
}
